export type Label = {
    x: number;
    y: number;
    id: number;
    text: string;
    color: { r: number, g: number, b: number, a: number },
};